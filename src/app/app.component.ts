import { Component } from '@angular/core';

@Component({
  selector: 'grind-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'grindhouse-movies';
}
