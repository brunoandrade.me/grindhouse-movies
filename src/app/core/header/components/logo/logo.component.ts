import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'grind-logo',
  templateUrl: './logo.component.html'
})
export class LogoComponent implements OnInit {
  @Input() logo: string = 'assets/imgs/main/logo-grindhouse.svg'
  
  constructor() { }

  ngOnInit() {
  }

}
