import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

const API = 'https://api.adorable.io/avatars/'

@Injectable({ providedIn: 'root'})
export class PerfilService{
    constructor(private http: HttpClient){}

    /**
     * ESTAVA PENSANDO EM FAZER UMA API PARA CONSUMIR OS DADOS
     * @param perfilNumber 
     */
    perfilUser(perfilNumber: number){
        return this.http.get<Object[]>(`${API}/${perfilNumber}/abott@adorable.png`)
    }
}