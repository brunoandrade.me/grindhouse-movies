import { Component, OnInit, Input } from '@angular/core';
import { PerfilService } from './perfil.service'

@Component({
  selector: 'grind-perfil',
  templateUrl: './perfil.component.html'
})
export class PerfilComponent implements OnInit {
  @Input() perfil: string = 'assets/imgs/main/perfil.png'

  constructor() { }

    ngOnInit() {

    }

 }
