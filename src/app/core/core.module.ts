import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { HeaderComponent } from "./header/header.component";
import { MenuComponent } from "./header/components/menu/menu.component";
import { LogoComponent } from './header/components/logo/logo.component';
import { PerfilComponent } from './header/components/perfil/perfil.component';
import { NotFoundComponent } from './erros/not-found/not-found.component';

@NgModule({
    declarations: [ 
        HeaderComponent, 
        MenuComponent, 
        LogoComponent, 
        PerfilComponent, 
        NotFoundComponent
    ],
    exports: [ HeaderComponent ],
    imports: [ HttpClientModule ]
})

export class CoreModule{}