import { Component, OnInit } from '@angular/core';
import { movies as listMovies } from './movies'

@Component({
  selector: 'grind-list-movies',
  templateUrl: './list-movies.component.html'
})
export class ListMoviesComponent implements OnInit {

  public movies: any[] = listMovies;
  rows: any[] = [];

  constructor() { }

  ngOnInit() {
    console.log(this.movies)
    this.rows = this.groupColumns(this.movies)

  }

  /**
   * @description função que coloca 
   * @param movies 
   */

  public groupColumns(movies){
    const newRows = []

    for(let index = 0; index < movies.length; index+=4){
      newRows.push(movies.slice(index, index + 4))
    }

    return newRows
  }


}
