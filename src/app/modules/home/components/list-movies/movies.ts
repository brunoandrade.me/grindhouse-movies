export const movies = [
    {
        id: 1, 
        cartaz: 'assets/imgs/movies/vingadores.png', 
        name: 'Vingadores: Guerra Infinita', 
        duration: '120 min',
        stars: 5,
        popular: false
    },
    {
        id: 2, 
        cartaz: 'assets/imgs/movies/brilho-eterno.png', 
        name: 'Brilho eterno de uma mente sem le...', 
        duration: '135 min',
        stars: 4,
        popular: false
    },
    {
        id: 3, 
        cartaz: 'assets/imgs/movies/John-Wick.png', 
        name: 'John Wick', 
        duration: '135 min',
        stars: 3,
        popular: false
    },
    {
        id: 4, 
        cartaz: 'assets/imgs/movies/Baby-Driver.png', 
        name: 'Baby Driver', 
        duration: '135 min',
        stars: 5,
        popular: false
    },
    {
        id: 5, 
        cartaz: 'assets/imgs/movies/Pantera-Negra.png', 
        name: 'Pantera Negra', 
        duration: '135 min',
        stars: 4,
        popular: false
    },
    {
        id: 6, 
        cartaz: 'assets/imgs/movies/Sicario.png', 
        name: 'Sicario', 
        duration: '135 min',
        stars: 2,
        popular: false
    },
    {
        id: 7, 
        cartaz: 'assets/imgs/movies/Corra.png', 
        name: 'Corra', 
        duration: '135 min',
        stars: 4,
        popular: false
    },
    {
        id: 8, 
        cartaz: 'assets/imgs/movies/Mae.png', 
        name: 'Mãe!', 
        duration: '135 min',
        stars: 4,
        popular: false
    },
    {
        id: 9, 
        cartaz: 'assets/imgs/movies/Logan.png', 
        name: 'Logan', 
        duration: '135 min',
        stars: 5,
        popular: true
    },
    {
        id: 10, 
        cartaz: 'assets/imgs/movies/Blade-Runner-2049.png', 
        name: 'Blade Runner 2049', 
        duration: '135 min',
        stars: 4,
        popular: true
    },
    {
        id: 10, 
        cartaz: 'assets/imgs/movies/Moonlight.png', 
        name: 'Moonlight', 
        duration: '135 min',
        stars: 3,
        popular: true
    },
    {
        id: 10, 
        cartaz: 'assets/imgs/movies/Dunkirk.png', 
        name: 'Dunkirk', 
        duration: '135 min',
        stars: 4,
        popular: true
    }
    
]