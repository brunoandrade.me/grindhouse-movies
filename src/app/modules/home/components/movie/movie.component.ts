import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'grind-movie',
  templateUrl: './movie.component.html'
})
export class MovieComponent implements OnInit {
  @Input() public name: string
  @Input() public cartaz: string
  @Input() public duration: string
  @Input() public stars: any
  public starsEl: string;

  constructor() {
    //this.countStars()
   }

  ngOnInit() {
    //this.countStars()
  }

  countStars(){
    for(let index = 0; index < this.stars; index++){
      this.starsEl  =  `<i class="fa fa-star">`
      console.log(this.starsEl)
    }
  }

}
