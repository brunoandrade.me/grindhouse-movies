import { NgModule } from "@angular/core";

import { HomeComponent } from './home/pages/home/home.component';
import { HeaderHomeComponent } from './home/components/header-home/header-home.component';
import { SearchComponent } from "../shared/components/search/search.component";
import { MovieComponent } from './home/components/movie/movie.component';
import { ListMoviesComponent } from './home/components/list-movies/list-movies.component';
import { CommonModule } from "@angular/common";


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        HomeComponent, 
        HeaderHomeComponent, 
        SearchComponent, MovieComponent, ListMoviesComponent, 
    ],
    exports: [ HomeComponent,  ]
})

export class ModulesModule {}